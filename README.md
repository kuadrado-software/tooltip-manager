# Tooltip Manager
## A simple and lightweight tool that allows you to manage tooltips in a web page.

__________________

An instance of `ToolTipsManager` will react to mousemove events over any element in the page providing an html5 `tooltip` attribute.
The tooltip attribute content will be displayed as a string in a small popup near the element.

The popup can be customize with any css property with the `style` parameter.

## Integration - usage

### Start the tooltips manager
The tooltpis manager is started simply by being instanciated so you'll just have to do:
```js
const my_tooltips_manager = new ToolTipsManager({
            style: { 
                backgroundColor: "#222d",
                borderRadius: "4px",
                border: "2px solid white",
                // any css property you like to pass to the tooltip popups
            },
            pop_tooltip_delay: 200, // a millisecond delay before the popups shows up. Minium is 50. The count starts when your mouse stops over an element.
        });
```

At this point the tooltips will start showing up !


### Destroy the instance
Is at some point of your script you want to remove the tooltips manager, you need to call the `clear()` method. This will remove all event listeners and timeout callback subscriptions so there is no dandling references of your instance laying around.
```js
my_tooltips_manager.clear()
```

### Duplicate instances
If a TooltipsManager is created and finds out another ToolTipsManager has been already created in the same window, it will automatically cancel itself.
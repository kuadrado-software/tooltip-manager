"use strict";

class ToolTipsManager {
    constructor(args = {}) {
        if (!window.tooltip_manager_registered) {
            this.init(args);
        } else {
            // Avoid having mutiple instances of this manager in the same window.
            console.warn("A TooltipManager instance already exists in that window, this one will be cancelled.");
            this.cancelled = true;
        }
    }

    init(args) {
        this.pop_tooltip_delay = Math.max(50, args.pop_tooltip_delay || 250);

        const style = Object.assign({
            backgroundColor: "#000c",
            color: "white",
            borderRadius: "8px",
            padding: "15px 20px",
            fontSize: "16px",
            maxWidth: "300px",
        }, args.style || {});

        this.tooltip_maxwidth = parseInt(style.maxWidth.replace("px", ""));

        this.state = {
            tooltip_target: document.createElement("div"),
            tooltip_text: "",
            tooltip_element: (function () {
                const el = document.createElement("div");
                el.style.position = "absolute";
                el.style.display = "none";
                Object.entries(style).forEach(kv => {
                    const [attr, value] = kv;
                    el.style[attr] = value;
                });
                document.body.appendChild(el);
                return el;
            })()
        };

        this.on_mouseleave_tooltip_element_listener = () => {
            this.state.tooltip_target.removeEventListener("mouseleave", this.on_mouseleave_tooltip_element_listener);
            this.clear_tooltip();
        };

        this.on_mousestop_listener = e_target => {
            if (e_target === this.state.tooltip_target) return;

            let recursion_state = 4;
            let tooltip_target = e_target;
            const find_tooltip = function (node) {
                if (!node) return undefined;

                if (node.tooltip) {
                    tooltip_target = node;
                    return node.tooltip;
                } else if (recursion_state > 0) {
                    recursion_state--;
                    const rec_find = find_tooltip(node.parentNode);

                    if (rec_find) {
                        return rec_find;
                    }

                } else return undefined
            };

            const tooltip = find_tooltip(e_target);

            if (tooltip) {
                this.state.tooltip_target = tooltip_target;
                const target_bounds = tooltip_target.getBoundingClientRect();

                if (target_bounds.left === 0 && target_bounds.top === 0) return;

                this.state.tooltip_text = tooltip;
                this.state.tooltip_element.innerHTML = this.state.tooltip_text;
                this.state.tooltip_element.style.display = "block";
                const el_bounds = this.state.tooltip_element.getBoundingClientRect();
                const el_height = el_bounds.height;
                const el_width = el_bounds.width;

                const left = parseInt(target_bounds.left);
                const top = parseInt(target_bounds.top - el_height - 5);
                const right = left + el_width;
                const window_width = document.documentElement.clientWidth;

                this.state.tooltip_element.style.left = (
                    window_width - right < 0 ?
                        left + (window_width - right)
                        : left
                ).toString() + "px";

                this.state.tooltip_element.style.top = parseInt(
                    (top < 0 ? target_bounds.bottom + 5 : target_bounds.top - el_height - 5) + window.scrollY
                ).toString() + "px";

                this.state.tooltip_target.addEventListener("mouseleave", this.on_mouseleave_tooltip_element_listener);
            } else if (this.state.tooltip_text !== ""
                && !Array.from(
                    this.state.tooltip_target.getElementsByTagName('*')
                ).slice(0, 4).includes(tooltip_target)) {

                this.clear_tooltip();
            }
        };

        this.listener_move = e => {
            if (this.move_timeout_id) {
                clearTimeout(this.move_timeout_id);
            }
            this.move_timeout_id = setTimeout(this.on_mousestop_listener.bind(this, e.target), this.pop_tooltip_delay);
        };

        window.addEventListener("mousemove", this.listener_move);
        window.tooltip_manager_registered = true;
    }

    clear_tooltip() {
        this.state.tooltip_target = document.createElement("div");
        this.state.tooltip_element.style.display = "none";
        this.state.tooltip_text = "";
    }

    clear() {
        if (this.cancelled) return;

        delete window.tooltip_manager_registered;

        this.state.tooltip_element.remove();

        if (this.move_timeout_id) {
            clearTimeout(this.move_timeout_id);
        }
        window.removeEventListener("mousemove", this.listener_move);
    }
}

module.exports = ToolTipsManager;